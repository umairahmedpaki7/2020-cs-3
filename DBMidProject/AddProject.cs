﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DBMidProject
{
    public partial class AddProject : Form
    {
        public AddProject()
        {
            InitializeComponent();
            warn.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (description.Text != "" && title.Text != "")
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Project values (@Description, @Title)", con);
                cmd.Parameters.AddWithValue("@Description", description.Text);
                cmd.Parameters.AddWithValue("@Title", title.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Data is added successfully");
            }
            else
            {
                warn.Show();
            }
        }
    }
}
