﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using System.Windows.Forms;

namespace DBMidProject
{
    public partial class UpdateDeleteAdvisor : Form
    {
        public UpdateDeleteAdvisor()
        {
            InitializeComponent();
            replaceID.Hide();
            replace.Hide();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT ID FROM Advisor", con);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd);
            DataSet ds3 = new DataSet();
            da3.Fill(ds3, "Group");
            id.ValueMember = "ID";
            id.DataSource = ds3.Tables["Group"];
            id.SelectedIndex = -1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Advisor.Id,Value as Designation,Salary from Advisor,Lookup where Advisor.Designation=Lookup.Id and Advisor.Id=@ID", con);
            cmd.Parameters.AddWithValue("@ID", id.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["ID"].ReadOnly = true;
        }
        Regex salary = new Regex("^[0-9]*$");
        private void button2_Click(object sender, EventArgs e)
        {
            if (salary.IsMatch(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].FormattedValue.ToString()))
            {
                if (dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString()=="Professor" || dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString() == "Associate Professor" || dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString() == "Assisstant Professor" || dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString() == "Lecturer" || dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString() == "Industry Professional")
                {
                    var con = Configuration.getInstance().getConnection();

                    SqlCommand cmd2 = new SqlCommand("select ID from Lookup where Category='DESIGNATION' and Value=@value", con);
                    cmd2.Parameters.AddWithValue("@value", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString());
                    cmd2.ExecuteNonQuery();

                    SqlCommand cmd = new SqlCommand("UPDATE Advisor SET Designation=@Designation,Salary=@Salary WHERE ID=@ID", con);
                    cmd.Parameters.AddWithValue("@ID", id.Text);
                    cmd.Parameters.AddWithValue("@Designation", cmd2.ExecuteScalar().ToString());
                    cmd.Parameters.AddWithValue("@Salary", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Updated");
                }
                else
                {
                    MessageBox.Show("Please correct the format of designation");
                }
            }
            else
            {
                MessageBox.Show("Please correct the format of salary");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (replaceID.Text == "")
            {
                var con = Configuration.getInstance().getConnection();

                SqlCommand cmd2 = new SqlCommand("select DISTINCT(@ID1) from Advisor,Lookup where @ID1 in(select AdvisorId from ProjectAdvisor)", con);
                cmd2.Parameters.AddWithValue("@ID1", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                cmd2.ExecuteNonQuery();
                var data = cmd2.ExecuteScalar();

                if (data == null)
                {
                    SqlCommand cmd = new SqlCommand("DELETE FROM Advisor WHERE ID=@ID", con);
                    cmd.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Deleted");
                }
                else
                {
                    SqlCommand cmd3 = new SqlCommand("select ID from Advisor where ID not in (SELECT distinct(ProjectAdvisor.AdvisorId) from (select distinct(Project.Id) from Project where @ID2 in  (select AdvisorId from ProjectAdvisor where ProjectId=Id )) as p join ProjectAdvisor on p.Id=ProjectAdvisor.ProjectId)", con);
                    cmd3.Parameters.AddWithValue("@ID2", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                    SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                    DataSet ds3 = new DataSet();
                    da3.Fill(ds3, "Group");
                    replaceID.ValueMember = "Id";
                    replaceID.DataSource = ds3.Tables["Group"];
                    replaceID.SelectedIndex = -1;
                    replace.Text = "The " + dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString() + " Advisor ID is serving as a advisor in other project so,please select another advisor for that groups.If no one availible then add";
                    replace.Show();
                    replaceID.Show();

                }
            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("UPDATE ProjectAdvisor SET AdvisorID=@AdvisorID where AdvisorID=@AdvisorID1", con);
                cmd.Parameters.AddWithValue("@AdvisorID", replaceID.Text);
                cmd.Parameters.AddWithValue("@AdvisorID1", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                cmd.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("DELETE FROM Advisor WHERE ID=@ID", con);
                cmd2.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
                cmd2.ExecuteNonQuery();
                MessageBox.Show("Successfully Deleted");
                replaceID.SelectedIndex = -1;

            }
        }

        private void UpdateDeleteAdvisor_Load(object sender, EventArgs e)
        {

        }

        private void replaceID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Please correct the format of update data");
        }
    }
}
