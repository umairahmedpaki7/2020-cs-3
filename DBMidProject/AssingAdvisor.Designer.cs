﻿
namespace DBMidProject
{
    partial class AssingAdvisor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.mainAdvisorID = new System.Windows.Forms.ComboBox();
            this.co_advisorID = new System.Windows.Forms.ComboBox();
            this.industryAdvisorID = new System.Windows.Forms.ComboBox();
            this.projectID = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(23, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(468, 164);
            this.dataGridView1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(521, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Main Advisor ID";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(23, 215);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 51;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(468, 150);
            this.dataGridView2.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(534, 284);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Select Project ID";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(669, 438);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "ASSIGN";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(521, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(169, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Select Industry Advisor ID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(521, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Select C0_Advisor ID";
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(23, 388);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowHeadersWidth = 51;
            this.dataGridView3.RowTemplate.Height = 24;
            this.dataGridView3.Size = new System.Drawing.Size(468, 150);
            this.dataGridView3.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(149, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "ADVISORS";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(149, 195);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "PROJECTS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(149, 368);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(253, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "ADVISORS ASSINGED TO PROJECTS";
            // 
            // mainAdvisorID
            // 
            this.mainAdvisorID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mainAdvisorID.FormattingEnabled = true;
            this.mainAdvisorID.Location = new System.Drawing.Point(707, 28);
            this.mainAdvisorID.Name = "mainAdvisorID";
            this.mainAdvisorID.Size = new System.Drawing.Size(121, 24);
            this.mainAdvisorID.TabIndex = 18;
            // 
            // co_advisorID
            // 
            this.co_advisorID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.co_advisorID.FormattingEnabled = true;
            this.co_advisorID.Location = new System.Drawing.Point(707, 91);
            this.co_advisorID.Name = "co_advisorID";
            this.co_advisorID.Size = new System.Drawing.Size(121, 24);
            this.co_advisorID.TabIndex = 19;
            // 
            // industryAdvisorID
            // 
            this.industryAdvisorID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.industryAdvisorID.FormattingEnabled = true;
            this.industryAdvisorID.Location = new System.Drawing.Point(707, 160);
            this.industryAdvisorID.Name = "industryAdvisorID";
            this.industryAdvisorID.Size = new System.Drawing.Size(121, 24);
            this.industryAdvisorID.TabIndex = 20;
            // 
            // projectID
            // 
            this.projectID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.projectID.FormattingEnabled = true;
            this.projectID.Location = new System.Drawing.Point(707, 284);
            this.projectID.Name = "projectID";
            this.projectID.Size = new System.Drawing.Size(121, 24);
            this.projectID.TabIndex = 21;
            // 
            // AssingAdvisor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1043, 645);
            this.Controls.Add(this.projectID);
            this.Controls.Add(this.industryAdvisorID);
            this.Controls.Add(this.co_advisorID);
            this.Controls.Add(this.mainAdvisorID);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "AssingAdvisor";
            this.Text = "AssingAdvisor";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox mainAdvisorID;
        private System.Windows.Forms.ComboBox co_advisorID;
        private System.Windows.Forms.ComboBox industryAdvisorID;
        private System.Windows.Forms.ComboBox projectID;
    }
}