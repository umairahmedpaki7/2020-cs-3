﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DBMidProject
{
    public partial class DeleteProject : Form
    {
        public DeleteProject()
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT ID FROM Project", con);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd);
            DataSet ds3 = new DataSet();
            da3.Fill(ds3, "Group");
            id.ValueMember = "ID";
            id.DataSource = ds3.Tables["Group"];
            id.SelectedIndex = -1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from project where ID=@ID", con);
            cmd.Parameters.AddWithValue("@ID", id.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["ID"].ReadOnly = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE project SET Description=@Description,Title=@Title", con);
            cmd.Parameters.AddWithValue("@Title", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString());
            cmd.Parameters.AddWithValue("@Description", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].FormattedValue.ToString());
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Updated");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("DELETE FROM ProjectAdvisor WHERE ProjectID=@ID", con);
            cmd2.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
            cmd2.ExecuteNonQuery();

            SqlCommand cmd3 = new SqlCommand("DELETE FROM GroupProject WHERE ProjectID=@ID", con);
            cmd3.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
            cmd3.ExecuteNonQuery();


            SqlCommand cmd = new SqlCommand("DELETE FROM project WHERE ID=@ID", con);
            cmd.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted");
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Please correct the format of update data");
        }
    }
}
