﻿
namespace DBMidProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.evaluationPane = new System.Windows.Forms.Panel();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.EVALUATION = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.ProjectSubmenu = new System.Windows.Forms.Panel();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.advisorSubMenu = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.addPanel = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.ShowData = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.childFormPanel = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.evaluationPane.SuspendLayout();
            this.ProjectSubmenu.SuspendLayout();
            this.advisorSubMenu.SuspendLayout();
            this.addPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(107)))), ((int)(((byte)(209)))));
            this.panel1.Controls.Add(this.button18);
            this.panel1.Controls.Add(this.evaluationPane);
            this.panel1.Controls.Add(this.EVALUATION);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.button13);
            this.panel1.Controls.Add(this.button12);
            this.panel1.Controls.Add(this.ProjectSubmenu);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.advisorSubMenu);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.addPanel);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(244, 674);
            this.panel1.TabIndex = 0;
            // 
            // button18
            // 
            this.button18.Dock = System.Windows.Forms.DockStyle.Top;
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ForeColor = System.Drawing.Color.White;
            this.button18.Location = new System.Drawing.Point(0, 684);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(223, 23);
            this.button18.TabIndex = 13;
            this.button18.Text = "EVALUATION OF GROUPS";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // evaluationPane
            // 
            this.evaluationPane.Controls.Add(this.button17);
            this.evaluationPane.Controls.Add(this.button16);
            this.evaluationPane.Controls.Add(this.button15);
            this.evaluationPane.Dock = System.Windows.Forms.DockStyle.Top;
            this.evaluationPane.Location = new System.Drawing.Point(0, 611);
            this.evaluationPane.Name = "evaluationPane";
            this.evaluationPane.Size = new System.Drawing.Size(223, 73);
            this.evaluationPane.TabIndex = 12;
            // 
            // button17
            // 
            this.button17.Dock = System.Windows.Forms.DockStyle.Top;
            this.button17.Location = new System.Drawing.Point(0, 46);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(223, 23);
            this.button17.TabIndex = 2;
            this.button17.Text = "UPDATE/DELETE";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.Dock = System.Windows.Forms.DockStyle.Top;
            this.button16.Location = new System.Drawing.Point(0, 23);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(223, 23);
            this.button16.TabIndex = 1;
            this.button16.Text = "SHOW DATA";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button15
            // 
            this.button15.Dock = System.Windows.Forms.DockStyle.Top;
            this.button15.Location = new System.Drawing.Point(0, 0);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(223, 23);
            this.button15.TabIndex = 0;
            this.button15.Text = "ADD";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // EVALUATION
            // 
            this.EVALUATION.Dock = System.Windows.Forms.DockStyle.Top;
            this.EVALUATION.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EVALUATION.ForeColor = System.Drawing.Color.White;
            this.EVALUATION.Location = new System.Drawing.Point(0, 577);
            this.EVALUATION.Name = "EVALUATION";
            this.EVALUATION.Size = new System.Drawing.Size(223, 34);
            this.EVALUATION.TabIndex = 11;
            this.EVALUATION.Text = "EVALUATION";
            this.EVALUATION.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.EVALUATION.UseVisualStyleBackColor = true;
            this.EVALUATION.Click += new System.EventHandler(this.EVALUATION_Click);
            // 
            // button14
            // 
            this.button14.Dock = System.Windows.Forms.DockStyle.Top;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.Location = new System.Drawing.Point(0, 543);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(223, 34);
            this.button14.TabIndex = 10;
            this.button14.Text = "ASSING GROUP";
            this.button14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button13
            // 
            this.button13.Dock = System.Windows.Forms.DockStyle.Top;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(0, 509);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(223, 34);
            this.button13.TabIndex = 9;
            this.button13.Text = "GROUPS MANAGEMENT";
            this.button13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Transparent;
            this.button12.Dock = System.Windows.Forms.DockStyle.Top;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Location = new System.Drawing.Point(0, 475);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(223, 34);
            this.button12.TabIndex = 8;
            this.button12.Text = "ASSING ADVISOR";
            this.button12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // ProjectSubmenu
            // 
            this.ProjectSubmenu.Controls.Add(this.button9);
            this.ProjectSubmenu.Controls.Add(this.button10);
            this.ProjectSubmenu.Controls.Add(this.button11);
            this.ProjectSubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.ProjectSubmenu.Location = new System.Drawing.Point(0, 402);
            this.ProjectSubmenu.Name = "ProjectSubmenu";
            this.ProjectSubmenu.Size = new System.Drawing.Size(223, 73);
            this.ProjectSubmenu.TabIndex = 7;
            // 
            // button9
            // 
            this.button9.Dock = System.Windows.Forms.DockStyle.Top;
            this.button9.Location = new System.Drawing.Point(0, 46);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(223, 23);
            this.button9.TabIndex = 5;
            this.button9.Text = "UPDATE/DELETE";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Dock = System.Windows.Forms.DockStyle.Top;
            this.button10.Location = new System.Drawing.Point(0, 23);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(223, 23);
            this.button10.TabIndex = 4;
            this.button10.Text = "SHOW DATA";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Dock = System.Windows.Forms.DockStyle.Top;
            this.button11.Location = new System.Drawing.Point(0, 0);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(223, 23);
            this.button11.TabIndex = 3;
            this.button11.Text = "ADD";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button8
            // 
            this.button8.Dock = System.Windows.Forms.DockStyle.Top;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(0, 368);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(223, 34);
            this.button8.TabIndex = 6;
            this.button8.Text = "PROJECT";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // advisorSubMenu
            // 
            this.advisorSubMenu.Controls.Add(this.button7);
            this.advisorSubMenu.Controls.Add(this.button6);
            this.advisorSubMenu.Controls.Add(this.button5);
            this.advisorSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.advisorSubMenu.Location = new System.Drawing.Point(0, 294);
            this.advisorSubMenu.Name = "advisorSubMenu";
            this.advisorSubMenu.Size = new System.Drawing.Size(223, 74);
            this.advisorSubMenu.TabIndex = 5;
            // 
            // button7
            // 
            this.button7.Dock = System.Windows.Forms.DockStyle.Top;
            this.button7.Location = new System.Drawing.Point(0, 46);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(223, 23);
            this.button7.TabIndex = 2;
            this.button7.Text = "UPDATE/DELETE";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Dock = System.Windows.Forms.DockStyle.Top;
            this.button6.Location = new System.Drawing.Point(0, 23);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(223, 23);
            this.button6.TabIndex = 1;
            this.button6.Text = "SHOW DATA";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.Location = new System.Drawing.Point(0, 0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(223, 23);
            this.button5.TabIndex = 0;
            this.button5.Text = "ADD";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(0, 260);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(223, 34);
            this.button4.TabIndex = 4;
            this.button4.Text = "ADVISOR";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // addPanel
            // 
            this.addPanel.Controls.Add(this.button3);
            this.addPanel.Controls.Add(this.ShowData);
            this.addPanel.Controls.Add(this.button2);
            this.addPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.addPanel.Location = new System.Drawing.Point(0, 160);
            this.addPanel.Name = "addPanel";
            this.addPanel.Size = new System.Drawing.Size(223, 100);
            this.addPanel.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(3, 67);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.button3.Size = new System.Drawing.Size(241, 28);
            this.button3.TabIndex = 2;
            this.button3.Text = "UPDATE/DELETE";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // ShowData
            // 
            this.ShowData.Dock = System.Windows.Forms.DockStyle.Top;
            this.ShowData.ForeColor = System.Drawing.Color.Black;
            this.ShowData.Location = new System.Drawing.Point(0, 36);
            this.ShowData.Name = "ShowData";
            this.ShowData.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.ShowData.Size = new System.Drawing.Size(223, 29);
            this.ShowData.TabIndex = 1;
            this.ShowData.Text = "SHOW DATA";
            this.ShowData.UseVisualStyleBackColor = true;
            this.ShowData.Click += new System.EventHandler(this.ShowData_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.button2.Size = new System.Drawing.Size(223, 36);
            this.button2.TabIndex = 0;
            this.button2.Text = "ADD";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(0, 126);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(223, 34);
            this.button1.TabIndex = 2;
            this.button1.Text = "STUDENT";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(107)))), ((int)(((byte)(209)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(223, 126);
            this.panel4.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(107)))), ((int)(((byte)(209)))));
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(244, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(898, 126);
            this.panel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(174, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(581, 48);
            this.label1.TabIndex = 0;
            this.label1.Text = "Project Management System";
            // 
            // childFormPanel
            // 
            this.childFormPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.childFormPanel.ForeColor = System.Drawing.Color.Black;
            this.childFormPanel.Location = new System.Drawing.Point(244, 126);
            this.childFormPanel.Name = "childFormPanel";
            this.childFormPanel.Size = new System.Drawing.Size(898, 548);
            this.childFormPanel.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1142, 674);
            this.Controls.Add(this.childFormPanel);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Project Management";
            this.panel1.ResumeLayout(false);
            this.evaluationPane.ResumeLayout(false);
            this.ProjectSubmenu.ResumeLayout(false);
            this.advisorSubMenu.ResumeLayout(false);
            this.addPanel.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel childFormPanel;
        private System.Windows.Forms.Panel addPanel;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button ShowData;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel advisorSubMenu;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel ProjectSubmenu;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Panel evaluationPane;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button EVALUATION;
        private System.Windows.Forms.Button button18;
    }
}

