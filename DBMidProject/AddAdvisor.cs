﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace DBMidProject
{
    public partial class AddAdvisor : Form
    {
        public AddAdvisor()
        {
            InitializeComponent();
            warn.Hide();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("Select Value  from [Lookup] where Category='Designation' ", con);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataSet ds3 = new DataSet();
            da3.Fill(ds3, "Value");
            designation.ValueMember = "Value";
            designation.DataSource = ds3.Tables["Value"];
            designation.SelectedIndex = -1;
            
        }
        Regex salaryRegex = new Regex("^[0-9]*$");
        private void button1_Click(object sender, EventArgs e)
        {
            if (designation.Text != "" && salary.Text != "" )
            {

                if (salaryRegex.IsMatch(salary.Text))
                {
                    var con = Configuration.getInstance().getConnection();

                    SqlCommand cmd2 = new SqlCommand("Select ID from Lookup where Category='DESIGNATION' AND Value=@value", con);
                    cmd2.Parameters.AddWithValue("Value", designation.Text);
                    cmd2.ExecuteNonQuery();

                    SqlCommand cmd3 = new SqlCommand("Select MAX(ID) from Advisor",con);
                    cmd3.ExecuteNonQuery();


                    SqlCommand cmd = new SqlCommand("Insert into Advisor values (@ID,@Designation, @Salary)", con);
                    cmd.Parameters.AddWithValue("@ID", int.Parse(cmd3.ExecuteScalar().ToString())+1);
                    cmd.Parameters.AddWithValue("@Designation", int.Parse(cmd2.ExecuteScalar().ToString()));
                    cmd.Parameters.AddWithValue("@Salary", salary.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Data is added successfully");
                }
                else
                {
                    MessageBox.Show("Please enter the salary in the correct format");
                }
            }
            else
            {
                warn.Show();
            }
        }
    }
}
